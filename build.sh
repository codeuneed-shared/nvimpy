#!/usr/bin/env bash
repo=compree

export dockerfile=dockerfile
export image="test"
export version="0.0.1"
declare -i push=0
export push

options=$(getopt -l "file:,image:,push,version:" -o "f:i:pv:" -- "$@")

eval set -- "$options"

while true
do
case $1 in
-f|--file)
    shift
    export dockerfile=$1
    ;;
-v|--version)
    shift
    export version=$1
    ;;
-i|--image)
    shift
    export image=$1
    ;;
-p|--push)
    let push=1
    export push
    ;;
--)
    shift
    break
    ;;
esac
shift
done

envimage=${image}:v${version}
repoenvimage=${repo}/${envimage}


DOCKER_BUILDKIT=1 docker build -f ${dockerfile} ${proxy_settings} -t ${envimage} .
docker tag ${envimage} ${repoenvimage}
if [ $push -gt 0 ]; then
    docker push ${repoenvimage}
fi


