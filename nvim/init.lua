require("confs")
require("lsp_config")
require("hop").setup()
local hopmap 
local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
local opts = { noremap=true, silent=true }
buf_set_keymap('n', 'g/', '<cmd>HopChar2<cr>',opts)
buf_set_keymap('n', 'g.', '<cmd>HopChar1<cr>',opts)

