local optset = {
expandtab = true,
shiftwidth = 4,
tabstop = 4,
rnu = true,
ic = true,
smartcase = true,
cmdheight = 2,
autoindent = true,
softtabstop = 4,
showmatch = true,
showmode = true,
showcmd = true,
cursorline = true,
ruler = true,
splitright = true,
listchars = {trail = "·", nbsp = "·", precedes = "«", extends = "»", eol = "↲", tab = "▸~" },
backspace = { indent, eol, start },
syntax = "enable"
}
-- copied / adapted from LunarVim
vim.opt.shortmess:append "c"
for k, v in pairs(optset) do
  vim.opt[k] = v
end
local opts = { noremap = true, silent = true }

local term_opts = { silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap
-- --Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

keymap("n", "<leader>e", ":Lex 30<cr>", opts)

keymap("n", "<M-Up>", ":resize +4<CR>", opts)
keymap("n", "<M-Down>", ":resize -4<CR>", opts)
keymap("n", "<M-Left>", ":vertical resize -4<CR>", opts)
keymap("n", "<M-Right>", ":vertical resize +4<CR>", opts)

-- in visual, keep indent outdent from > and <
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)
keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
keymap("x", "K", ":move '<-2<CR>gv-gv", opts)

keymap("n", "<S-l>", ":bnext<CR>", opts)
keymap("n", "<S-h>", ":bprevious<CR>", opts)
-- Terminal switching
keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)

keymap("i", "jj","<esc>", opts)
keymap("n", "<leader><space>",":noh<cr>", opts)
keymap("n", ";",":", opts)
keymap("n", ":",";", opts)
keymap("n", "<leader>w","<c-w>v<c-w>l", opts)
