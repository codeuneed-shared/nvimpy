FROM python:3.8-buster AS builder_base

WORKDIR /projects
RUN apt-get update && apt-get install -y --allow-unauthenticated \
 python3-dev \
 curl \
 gnupg \
 git \
 wget \
 bsdmainutils \
 tree
# install ack directly
RUN curl https://beyondgrep.com/ack-v3.5.0 > /usr/bin/ack && chmod 0755 /usr/bin/ack

# TODO Python LSP

RUN apt-get update && apt-get install -y --allow-unauthenticated \
 software-properties-common \
 apt-utils pkg-config


RUN curl -LO https://github.com/neovim/neovim/releases/download/v0.6.1/nvim.appimage && \
    chmod u+x nvim.appimage && \
    ./nvim.appimage --appimage-extract && \
    ./squashfs-root/AppRun --version

# Optional: exposing nvim globally.
RUN mv squashfs-root / && \
    ln -s /squashfs-root/AppRun /usr/bin/nvim
RUN rm nvim.appimage

COPY .bashrc /root/.bashrc
COPY .inputrc /root/.inputrc
RUN mkdir -p /root/.config/nvim
COPY nvim/ /root/.config/nvim/

RUN dir="/root/.local/share/nvim/site/pack/genstuff/start/" && \
mkdir -p "$dir" && \
cd "$dir" && \
git clone --depth=1 --branch=master https://github.com/tpope/vim-commentary.git && \
cd vim-commentary && rm -rf .git

RUN dir="/root/.local/share/nvim/site/pack/gostuff/start/" && \
mkdir -p "$dir" && \
cd "$dir" && \
git clone --depth=1 --branch=master https://github.com/neovim/nvim-lspconfig.git  && \
cd nvim-lspconfig && rm -rf .git

RUN dir="/root/.local/share/nvim/site/pack/newstuff/start/" && \
mkdir -p "$dir" && \
cd "$dir" && \
git clone --depth=1 --branch=v1.2 https://github.com/phaazon/hop.nvim.git && \
cd hop.nvim && \
rm -rf .git


ENTRYPOINT ["bash"]
